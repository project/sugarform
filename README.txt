
Description:
------------

This module provides a way of creating enquiry forms to use the
leadCapture.php script in SugarCRM.

The leadCapture.php has proven to be more reliable than the SOAP method.

Features:
---------

* Multiple sugarform pages are configureable. Each page has its own settings,
  so e.g. you can create forms for different areas of your site.
* Configurable fields on the web form (First Name, Second Name, Company,
  Phone Number, Message)
* If the visitor is a registered user of the site, then the sender's
  email address field and sender's first name default to the values entered
  upon registration
* Configurable instructions/guidelines text above the form 
* Works with clean URLs as well as regular URLs
* Simple, the form is submitted stright to the SugarCRM leadCapture.php
  script, or simliar, then redirected to your desired destination.
* Validation is carried out by SugarCRM.

Database:
---------

With 5.0 and later this module uses own database tables, which are automatically
created for you.

Installation:
-------------

Please see the INSTALL document for details.

Bugs/Features/Patches:
----------------------

If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/sugarform

Author
------

Ashley Maher (ashley.maher@didymodesigns.com.au)

Sponsors
--------

Didymo Designs (http://www.didymodesigns.com.au)
